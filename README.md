# Car Search

A Universal React app using react, redux, next.js, jest and styled components.

### Install

```sh
yarn install
```

### Run the app

```sh
yarn dev
```

### Linting

```sh
yarn lint
```

### Tests

Run all tests:

```sh
yarn test
```

Run server tests:

```sh
yarn test:unit:server
```

Run client tests:

```sh
yarn test:unit:client
```
