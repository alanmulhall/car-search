import './polyfills.js'; // eslint-disable-line
import 'jest-styled-components';
import fetch from 'isomorphic-fetch';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

global.fetch = fetch;
