const express = require('express');
const path = require('path');
const next = require('next');
const apiRouteHandler = require('./routes/api/v1');
const appDynamicRoutes = require('./routes/app/routes');
const compression = require('compression');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const staticDir = path.resolve(__dirname, '.next/static');
const server = express();

const appDynamicRouteHandler = appDynamicRoutes.getRequestHandler(
  app,
  ({ req, res, route, query }) => {
    app.render(req, res, route.page, query);
  }
);

server.use(compression());
server.use('/_next/static', express.static(staticDir));
server.use(apiRouteHandler);
server.use(appDynamicRouteHandler);
server.get('*', (req, res) => handle(req, res));

module.exports = app.prepare().then(() => {
  return server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line
  });
});
