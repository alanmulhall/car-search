const routes = require('next-routes')();

routes.add('make', '/make/model/:id');

module.exports = routes;
