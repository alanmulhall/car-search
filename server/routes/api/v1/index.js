const router = require('express').Router();

router.use('/api/v1', require('./offer'));
router.use('/api/v1', require('./model'));
router.use('/api/v1', require('./models'));
router.use('/api/v1', require('./makes'));
router.use('/api', require('./404'));

module.exports = router;
