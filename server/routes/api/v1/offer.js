const express = require('express');
const carOfTheWeek = require('../../../data/carOfTheWeek.json');

const router = express.Router();

router.get('/offer', (req, res) => res.json(carOfTheWeek));

module.exports = router;
