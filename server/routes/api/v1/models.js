const express = require('express');
const models = require('../../../data/models.json');

const router = express.Router();

router.get('/models', (req, res) => res.json(models));

module.exports = router;
