const express = require('express');
const models = require('../../../data/models.json');

const router = express.Router();

router.get('/model/:id', (req, res) => {
  let response;
  const { params: { id } } = req;
  const model = models.filter(item => item.id === Number(id));
  if (model.length) {
    response = res.json(model[0]);
  } else {
    response = res.status(404).json({ error: 'Route not found' });
  }
  return response;
});

module.exports = router;
