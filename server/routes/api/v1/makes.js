const express = require('express');

const router = express.Router();
const makes = require('../../../data/makes.json');

router.get('/makes', (req, res) => res.json(makes));

module.exports = router;
