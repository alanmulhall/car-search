const express = require('express');

const router = express.Router();

router.get('/*', (req, res) => {
  return res.status(404).json({ error: 'Route not found' });
});

module.exports = router;
