const chai = require('chai');
const fetch = require('node-fetch');
const app = require('../../../../index');

const { expect } = chai;
const API_V1_404_URL = 'http://localhost:3000/api/v1/junk';
let httpServer;
let res;
let data;

describe('API 404', () => {
  describe('GET "/api/v1/junk"', () => {
    before(async () => {
      httpServer = await app;
      res = await fetch(API_V1_404_URL);
      data = await res.json();
    });

    it('request fails and has the correct status code', async () => {
      expect(await res.status).to.equal(404);
    });

    it('has the correct error message', async () => {
      expect(await data.error).to.be.equal('Route not found');
    });

    after(() => {
      httpServer.close();
    });
  });
});
