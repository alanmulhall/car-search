const chai = require('chai');
const fetch = require('node-fetch');
const app = require('../../../../index');

const { expect } = chai;
const API_V1_MAKES_URL = 'http://localhost:3000/api/v1/makes';
let httpServer;
let res;
let data;

before(async () => {
  httpServer = await app;
  res = await fetch(API_V1_MAKES_URL);
  data = await res.json();
});

describe('GET "/api/v1/makes"', () => {
  it('request was successful and has the correct status code', async () => {
    expect(await res.status).to.equal(200);
  });

  it('returns an array of makes', async () => {
    expect(await data).to.be.a('array');
  });

  describe('make', () => {
    it('has a "id"', async () => {
      expect(await data[0].id).to.exist; // eslint-disable-line
    });

    it('has a "name"', async () => {
      expect(await data[0].name).to.exist; // eslint-disable-line
    });
  });
});

after(() => {
  httpServer.close();
});
