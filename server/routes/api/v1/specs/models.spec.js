const chai = require('chai');
const fetch = require('node-fetch');
const app = require('../../../../index');

const { expect } = chai;
const API_V1_MODELS_URL = 'http://localhost:3000/api/v1/models';
let httpServer;
let res;
let data;

before(async () => {
  httpServer = await app;
  res = await fetch(API_V1_MODELS_URL);
  data = await res.json();
});

describe('GET "/api/v1/models"', () => {
  it('request was successful and has the correct status code', async () => {
    expect(await res.status).to.equal(200);
  });

  it('returns an array of models', async () => {
    expect(await data).to.be.a('array');
  });

  describe('model', () => {
    it('has a "id"', async () => {
      expect(await data[0].id).to.exist; // eslint-disable-line no-unused-expressions
    });

    it('has a "makeId"', async () => {
      expect(await data[0].makeId).to.exist; // eslint-disable-line no-unused-expressions
    });

    it('has a "name"', async () => {
      expect(await data[0].name).to.exist; // eslint-disable-line no-unused-expressions
    });

    it('has a "price"', async () => {
      expect(await data[0].price).to.exist; // eslint-disable-line no-unused-expressions
    });

    it('has a "imageUrl"', async () => {
      expect(await data[0].imageUrl).to.exist; // eslint-disable-line no-unused-expressions
    });
  });
});

after(() => {
  httpServer.close();
});
