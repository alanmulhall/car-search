const chai = require('chai');
const fetch = require('node-fetch');
const app = require('../../../../index');

const { expect } = chai;
const API_V1_MODEL_URL = 'http://localhost:3000/api/v1/model/300';
let httpServer;
let res;
let data;

before(async () => {
  httpServer = await app;
  res = await fetch(API_V1_MODEL_URL);
  data = await res.json();
});

describe('GET "/api/v1/model/:id"', () => {
  it('request was successful and has the correct status code', async () => {
    expect(await res.status).to.equal(200);
  });

  it('returns data in the shape of an object', async () => {
    expect(await data).to.be.a('object');
  });

  // TODO instead of checking the keys - check the entire object deep.equal

  it('has a "id"', async () => {
    expect(await data.id).to.exist; // eslint-disable-line no-unused-expressions
  });

  it('has a "makeId"', async () => {
    expect(await data.makeId).to.exist; // eslint-disable-line no-unused-expressions
  });

  it('has a "name"', async () => {
    expect(await data.name).to.exist; // eslint-disable-line no-unused-expressions
  });

  it('has a "price"', async () => {
    expect(await data.price).to.exist; // eslint-disable-line no-unused-expressions
  });

  it('has a "imageUrl"', async () => {
    expect(await data.imageUrl).to.exist; // eslint-disable-line no-unused-expressions
  });
});

after(() => {
  httpServer.close();
});
