const chai = require('chai');
const fetch = require('node-fetch');
const app = require('../../../../index');

const { expect } = chai;
const API_V1_OFFER_URL = 'http://localhost:3000/api/v1/offer';
let httpServer;
let res;
let data;

before(async () => {
  httpServer = await app;
  res = await fetch(API_V1_OFFER_URL);
  data = await res.json();
});

describe('GET "/api/v1/offer"', () => {
  it('request was successful and has the correct status code', async () => {
    expect(await res.status).to.equal(200);
  });

  it('returns data in the shape of an object', async () => {
    expect(await data).to.be.a('object');
  });

  it('has a "modelId"', async () => {
    expect(await data.modelId).to.exist; // eslint-disable-line no-unused-expressions
  });

  it('has a "review"', async () => {
    expect(await data.review).to.exist; // eslint-disable-line no-unused-expressions
  });
});

after(() => {
  httpServer.close();
});
