import React, { Component } from 'react';
import withRedux from 'next-redux-wrapper';
import { weeklyOfferfetch } from '../client/actions/actionCreators';
import { configureStore } from '../client/store/configureStore';
import Layout from '../client/components/Layout';
import OfferContainer from '../client/containers/OfferContainer';

class Page extends Component {
  static async getInitialProps({ store }) {
    await store.dispatch(weeklyOfferfetch());
    return {};
  }

  render() {
    return (
      <Layout>
        <OfferContainer />
      </Layout>
    );
  }
}

export default withRedux(configureStore)(Page); // eslint-disable-lint
