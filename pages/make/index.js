import React, { Component } from 'react';
import withRedux from 'next-redux-wrapper';
import { configureStore } from '../../client/store/configureStore';
import { modelFetch } from '../../client/actions/actionCreators';
import ModelContainer from '../../client/containers/ModelContainer';
import Layout from '../../client/components/Layout/Layout';

class MakePage extends Component {
  static async getInitialProps({ store, query: { id } }) {
    await store.dispatch(modelFetch(id));
    return {};
  }

  render() {
    return (
      <Layout>
        <ModelContainer />
      </Layout>
    );
  }
}

export default withRedux(configureStore)(MakePage); // eslint-disable-line
