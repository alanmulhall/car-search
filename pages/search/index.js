import React, { Component } from 'react';
import withRedux from 'next-redux-wrapper';
import { configureStore } from '../../client/store/configureStore';
import { makesFetch, modelsFetch } from '../../client/actions/actionCreators';
import Layout from '../../client/components/Layout';
import SearchContainer from '../../client/containers/SearchContainer';

class SearchPage extends Component {
  static async getInitialProps({ store }) {
    await Promise.all([
      store.dispatch(makesFetch()),
      store.dispatch(modelsFetch())
    ]);
    return {};
  }

  render() {
    return (
      <Layout>
        <SearchContainer />
      </Layout>
    );
  }
}

export default withRedux(configureStore)(SearchPage); // eslint-disable-line
