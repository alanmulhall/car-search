import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';
import { ServerStyleSheet, injectGlobal } from 'styled-components';

/* eslint-disable */

injectGlobal`
  html {
    box-sizing: border-box;
  }
  
  body {
    background: white; 
    font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;   
    margin: 0;
  }
  
  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }
`;

/* eslint-enabl e */

class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => props =>
      sheet.collectStyles(<App {...props} />)
    );
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  render() {
    return (
      <html lang="en">
        <Head>
          <title>Car Search</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          {this.props.styleTags}
        </Head>
        <body>
          <Main role="main" />
          <NextScript />
        </body>
      </html>
    );
  }
}

export default MyDocument; // eslint-disable-line
