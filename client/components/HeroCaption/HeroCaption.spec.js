import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import HeroCaption from './HeroCaption';

describe('<HeroCaption />', () => {
  it('renders correctly', () => {
    const mockHeroCaption = {
      review: 'The Mazda MX-5 is a traditional two-seat sports car.'
    };
    const component = renderer.create(<HeroCaption {...mockHeroCaption} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
