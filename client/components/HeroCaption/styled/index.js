import styled from 'styled-components';

/* eslint-disable */

export const CaptionStyled = styled.div`
  background: black;
  color: white;
  text-align: center;
  
  p {
    line-height: 1.5em;
  }
`;

/* eslint-enable */
