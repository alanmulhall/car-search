import React from 'react';
import PropTypes from 'prop-types';
import { Container, Col, Row } from 'react-grid-system';
import { CaptionStyled } from './styled';

const propTypes = {
  review: PropTypes.string.isRequired
};

const HeroCaption = ({ review }) => {
  return (
    <CaptionStyled>
      <Container>
        <Row>
          <Col>
            <p>{review}</p>
          </Col>
        </Row>
      </Container>
    </CaptionStyled>
  );
};

HeroCaption.propTypes = propTypes;

export default HeroCaption;
