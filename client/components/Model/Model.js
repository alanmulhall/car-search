import React from 'react';
import PropTypes from 'prop-types';
import { Container, Col, Row } from 'react-grid-system';
import Hero from '../Hero';
import ModelDetails from '../ModelDetails';

const propTypes = {
  id: PropTypes.number.isRequired,
  imageUrl: PropTypes.string.isRequired,
  makeId: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

const Model = ({ id, imageUrl, makeId, name, price }) => {
  return (
    <Hero imageUrl={imageUrl} id={id} name={name}>
      <Container>
        <Row>
          <Col>
            <ModelDetails
              id={id}
              imageUrl={imageUrl}
              makeId={makeId}
              name={name}
              price={price}
            />
          </Col>
        </Row>
      </Container>
    </Hero>
  );
};

Model.propTypes = propTypes;

export default Model;
