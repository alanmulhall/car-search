import React from 'react';
import renderer from 'react-test-renderer';
import Router from 'next/router';
import 'jest-styled-components';
import mockRouter from '../../shared/tests/mocks/mockRouter';
import Navigation from './Navigation';

Router.router = mockRouter;

describe('<Navigation />', () => {
  it('renders correctly', () => {
    const component = renderer.create(<Navigation />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
