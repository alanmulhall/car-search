import styled from 'styled-components';

/* eslint-disable */

export const UlStyled = styled.ul`
  display: flex;
  list-style-type: none;
  margin: 0;
  text-align: right;
  
  li {
    align-self: center;
  }
`;

export const LiStyled = styled.li`
  display: inline-block;
  margin-right: 5px;
  
  @media (min-width: 576px) { // small https://v4-alpha.getbootstrap.com/layout/overview/#responsive-breakpoints
     margin-right: 10px;
  }
  
  @media (min-width: 768px) { // medium
     margin-right: 20px;
  }
  
  a {
    color: #fff;
    text-decoration: none;
    
    &:hover {
      text-decoration: underline;
    }
  } 
`;

/* eslint-enable */
