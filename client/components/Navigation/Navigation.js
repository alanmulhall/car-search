import React from 'react';
import Link from 'next/link';
import { LiStyled, UlStyled } from './styled/NavigationStyled';

const Navigation = () => {
  return (
    <UlStyled>
      <LiStyled>
        <Link prefetch href={{ pathname: '/' }}>
          <a>Home</a>
        </Link>
      </LiStyled>
      <LiStyled>
        <Link prefetch href={{ pathname: '/search' }}>
          <a>Search</a>
        </Link>
      </LiStyled>
    </UlStyled>
  );
};

export default Navigation;
