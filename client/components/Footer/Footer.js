import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import { FooterStyled } from './styled';
import { QANTAS, QANTAS_LOGO } from '../../constants/app';

const Footer = () => {
  return (
    <FooterStyled role="contentinfo">
      <Container>
        <Row>
          <Col>
            <img src={QANTAS_LOGO} alt={QANTAS} />
          </Col>
        </Row>
      </Container>
    </FooterStyled>
  );
};

export default Footer;
