import styled from 'styled-components';

/* eslint-disable */

export const FooterStyled = styled.footer`
  background: #333;
  color: white;
  padding: 16px 24px;
  width: 100%;
  
  img {
    width: 117px;
  }
`;

/* eslint-enable */
