import React from 'react';
import renderer from 'react-test-renderer';
import Router from 'next/router';
import 'jest-styled-components';
import mockRouter from '../../shared/tests/mocks/mockRouter';
import Layout from './Layout';

Router.router = mockRouter;

describe('<Layout />', () => {
  it('renders correctly', () => {
    const component = renderer.create(
      <Layout>
        <p>Hello</p>
      </Layout>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
