import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import Hero from './Hero';

describe('<Hero />', () => {
  it('renders correctly when passed props "imageUrl" and "name"', () => {
    const mockWeeklyOffer = {
      id: 502,
      imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
      name: 'MX-5'
    };
    const component = renderer.create(<Hero {...mockWeeklyOffer} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly when passed props "imageUrl"', () => {
    const mockWeeklyOffer = {
      imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
    };
    const component = renderer.create(<Hero {...mockWeeklyOffer} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
