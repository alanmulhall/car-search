import styled from 'styled-components';

/* eslint-disable */

export const HeroStyled = styled.div`
  align-items: center;
  background-color: black;
  background-image: url(${props => props.imageUrl});
  background-position: center center;
  background-size: cover;
  box-sizing: border-box;
  display: flex;
  height: calc(100vh - 105px);
  min-height: 500px;
  overflow: hidden;
  position: relative;
  width: 100%;
  z-index: 0;
   
  h1 {
    background: black;
    color: white;
    display: inline-block;
    padding: 8px 24px;
    position: absolute;
    right: 0;
    top: 40px;
  }
`;

/* eslint-enable */
