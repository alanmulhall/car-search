import React from 'react';
import PropTypes from 'prop-types';
import { Link } from '../../../server/routes/app/routes';
import { HeroStyled } from './styled';

const defaultProps = {
  children: null,
  id: null,
  name: null
};

const propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  id: PropTypes.number,
  imageUrl: PropTypes.string.isRequired,
  name: PropTypes.string
};

const Hero = ({ children, id, imageUrl, name }) => {
  return (
    <HeroStyled imageUrl={imageUrl}>
      {id &&
        name && (
          <Link route="make" params={{ id }}>
            <a>
              <h1>{name}</h1>
            </a>
          </Link>
        )}
      {children}
    </HeroStyled>
  );
};

Hero.defaultProps = defaultProps;
Hero.propTypes = propTypes;

export default Hero;
