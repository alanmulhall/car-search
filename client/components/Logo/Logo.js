import React from 'react';
import Link from 'next/link';
import { APP_LOGO, APP_NAME } from '../../constants/app';

const Logo = () => {
  return (
    <Link prefetch href={{ pathname: '/' }}>
      <a rel="nofollow">
        <img src={APP_LOGO} alt={APP_NAME} />
      </a>
    </Link>
  );
};

export default Logo;
