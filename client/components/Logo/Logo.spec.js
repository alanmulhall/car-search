import React from 'react';
import renderer from 'react-test-renderer';
import Router from 'next/router';
import 'jest-styled-components';
import mockRouter from '../../shared/tests/mocks/mockRouter';
import Logo from './Logo';

Router.router = mockRouter;

describe('<Logo />', () => {
  it('renders correctly', () => {
    const component = renderer.create(<Logo />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
