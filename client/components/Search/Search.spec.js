import React from 'react';
import { shallow } from 'enzyme';
import Search from './Search';

const makes = [
  {
    id: 10,
    name: 'Porsche'
  },
  {
    id: 20,
    name: 'Nissan'
  }
];
const models = [
  {
    id: 100,
    makeId: 10,
    name: '911 Carrera 4s',
    price: 297130,
    imageUrl: 'http://files1.porsche.com/filestore/image/multimedia/none/991-2nd-c4s-modelimage-sideshot/model/15bd09cf-553b-11e5-8c32-0019999cd470;s25/porsche-model.png' // eslint-disable-line
  },
  {
    id: 110,
    makeId: 10,
    name: 'Cayenne GTS',
    price: 171605,
    imageUrl: 'http://files3.porsche.com/filestore/image/multimedia/none/rd-2013-9pa-e2-2nd-gts-modelimage-sideshot/model/c287d350-5920-11e4-99aa-001a64c55f5c;s25/porsche-model.png' // eslint-disable-line
  },
  {
    id: 120,
    makeId: 10,
    name: 'Panamera 4S',
    price: 328160,
    imageUrl: 'http://files1.porsche.com/filestore/image/multimedia/none/970-g2-4s-modelimage-sideshot/model/a23b6da0-33b9-11e6-9225-0019999cd470;s25/porsche-model.png' // eslint-disable-line
  }
];
let wrapper;

describe('<Search />', () => {
  beforeEach(() => {
    wrapper = shallow(<Search makes={makes} models={models} />);
  });

  it('search button is initially disabled', () => {
    const searchButtonDisabledValue = wrapper
      .find('#test-search')
      .prop('disabled');
    expect(searchButtonDisabledValue).toBeTruthy();
  });

  it('selecting a make updates the models select options', () => {
    wrapper.find('#test-makes').simulate('change', { target: { value: '10' } });
    const numberOfOptions = wrapper.find('#test-models option').length;
    expect(numberOfOptions).toEqual(4);
  });

  it('selecting a make and model activates the search button', () => {
    wrapper.find('#test-makes').simulate('change', { target: { value: '10' } });
    wrapper
      .find('#test-models')
      .simulate('change', { target: { value: '100' } });
    const searchButtonDisabledValue = wrapper
      .find('#test-search')
      .prop('disabled');
    expect(searchButtonDisabledValue).toBeFalsy();
  });
});
