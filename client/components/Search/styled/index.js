import styled from 'styled-components';
import { Select } from 'rebass';

/* eslint-disable */

export const FormStyled = styled.form`
  display: flex;
  
  > div {
    display: flex;
  }
`;

export const InputStyled = styled.input`
  background: green;
  color: white;
  cursor: pointer;
  padding: 10px 20px;
  text-transform: uppercase;
  
  &[disabled] {
    background: #ccc;
  }
`;

export const SelectStyled = styled(Select)`
  margin-right: 10px;
  width: 150px;
`;

/* eslint-enable */
