import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Col, Row } from 'react-grid-system';
import { Router } from '../../../server/routes/app/routes';
import { BoxStyled } from '../../shared/styled';
import { FormStyled, InputStyled, SelectStyled } from './styled';
import Hero from '../Hero';

const propTypes = {
  makes: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  models: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      imageUrl: PropTypes.string.isRequired,
      makeId: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

class Search extends Component {
  state = {
    isValid: false,
    makeId: '',
    modelId: '',
    models: []
  };

  handleSubmit = e => {
    e.preventDefault();
    const { modelId } = this.state;
    return Router.pushRoute(`/make/model/${modelId}`);
  };

  handleMakeChange = e => {
    const { models } = this.props;
    return this.setState({ makeId: Number(e.target.value) }, () => {
      const { makeId } = this.state;
      this.setState({
        models: models.filter(model => model.makeId === makeId)
      });
    });
  };

  handleModelChange = e => {
    const { makeId } = this.state;
    return this.setState({ modelId: Number(e.target.value) }, () => {
      const { modelId } = this.state;
      this.setState({
        isValid: !!(makeId && modelId)
      });
    });
  };

  render() {
    const { isValid, models } = this.state;
    const { makes } = this.props;
    const imageUrl = '/static/heros/coast-road.jpg';
    return (
      <Hero imageUrl={imageUrl}>
        <Container>
          <Row>
            <Col>
              <BoxStyled>
                <FormStyled id="test-form" onSubmit={this.handleSubmit}>
                  <SelectStyled
                    id="test-makes"
                    onChange={this.handleMakeChange}
                    value={this.state.make}
                  >
                    <option value="">Choose a make</option>
                    {makes.map(make => (
                      <option key={make.id} value={make.id}>
                        {make.name}
                      </option>
                    ))}
                  </SelectStyled>
                  <SelectStyled
                    id="test-models"
                    onChange={this.handleModelChange}
                    value={this.state.model}
                  >
                    <option value="">Select a model</option>
                    {models.map(model => (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    ))}
                  </SelectStyled>
                  <InputStyled
                    id="test-search"
                    type="submit"
                    value="Search"
                    disabled={!isValid}
                  />
                </FormStyled>
              </BoxStyled>
            </Col>
          </Row>
        </Container>
      </Hero>
    );
  }
}

Search.propTypes = propTypes;

export default Search;
