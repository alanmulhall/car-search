import styled from 'styled-components';

export const FigureStyled = styled.figure`
  float: left;
  margin: 0 20px 0 0;
`;

export const FigcaptionStyled = styled.figcaption`
  float: left;
`;
