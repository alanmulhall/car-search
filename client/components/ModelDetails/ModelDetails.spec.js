import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import ModelDetails from './ModelDetails';

describe('<ModelDetails />', () => {
  it('renders correctly', () => {
    const mockModel = {
      id: 520,
      imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
      makeId: 50,
      name: 'MX-5',
      price: 90000
    };
    const component = renderer.create(<ModelDetails {...mockModel} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
