import React from 'react';
import PropTypes from 'prop-types';
import { FigureStyled, FigcaptionStyled } from './styled';
import { BoxStyled } from '../../shared/styled';
import formatCurrency from '../../utils/formatCurrency';

const propTypes = {
  id: PropTypes.number.isRequired,
  imageUrl: PropTypes.string.isRequired,
  makeId: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

const ModelDetails = ({ id, imageUrl, makeId, name, price }) => {
  return (
    <BoxStyled>
      <FigureStyled>
        <img src={imageUrl} alt={name} width="150" />
      </FigureStyled>
      <FigcaptionStyled>
        <p>
          <strong>id:</strong> {id}
        </p>
        <p>
          <strong>makeId:</strong> {makeId}
        </p>
        <p>
          <strong>name:</strong> {name}
        </p>
        <p>
          <strong>price:</strong> ${formatCurrency(price)}
        </p>
      </FigcaptionStyled>
    </BoxStyled>
  );
};

ModelDetails.propTypes = propTypes;

export default ModelDetails;
