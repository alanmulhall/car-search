import React from 'react';
import PropTypes from 'prop-types';
import Hero from '../Hero';
import HeroCaption from '../HeroCaption';

const propTypes = {
  imageUrl: PropTypes.string.isRequired,
  modelId: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired
};

const Offer = ({ imageUrl, modelId, name, review }) => {
  return (
    <div>
      <Hero imageUrl={imageUrl} id={modelId} name={name} />
      <HeroCaption review={review} />
    </div>
  );
};

Offer.propTypes = propTypes;

export default Offer;
