import React from 'react';
import renderer from 'react-test-renderer';
import Router from 'next/router';
import 'jest-styled-components';
import mockRouter from '../../shared/tests/mocks/mockRouter';
import Offer from './Offer';

Router.router = mockRouter;

describe('<Offer />', () => {
  it('renders correctly', () => {
    const offer = {
      imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
      makeId: 50,
      modelId: 520,
      name: 'MX-5',
      review: 'The Mazda MX-5 is a traditional two-seat sports car.',
      price: 90000
    };
    const component = renderer.create(<Offer {...offer} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
