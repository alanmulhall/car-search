import styled from 'styled-components';

/* eslint-disable */

export const HeaderStyled = styled.header`
  background: #e40000;
  
  a {
    overflow: hidden;
  }
  
  img {
    max-width: 310px;
    width: 100%;
  }
`;

export const NavStyled = styled.nav`
  display: flex;
  height: 100%;
  justify-content: flex-end;
`;

/* eslint-enable */
