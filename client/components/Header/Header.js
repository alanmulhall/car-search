import React from 'react';
import { Container, Col, Row } from 'react-grid-system';
import { HeaderStyled, NavStyled } from './styled';
import Logo from '../Logo';
import Navigation from '../Navigation';

const Header = () => {
  return (
    <HeaderStyled role="banner">
      <Container>
        <Row>
          <Col xs={7} sm={6}>
            <Logo />
          </Col>
          <Col xs={5} sm={6}>
            <NavStyled role="navigation">
              <Navigation />
            </NavStyled>
          </Col>
        </Row>
      </Container>
    </HeaderStyled>
  );
};

export default Header;
