import React from 'react';
import renderer from 'react-test-renderer';
import Router from 'next/router';
import 'jest-styled-components';
import mockRouter from '../../shared/tests/mocks/mockRouter';
import Header from './Header';

Router.router = mockRouter;

describe('<Header />', () => {
  it('renders correctly', () => {
    const component = renderer.create(<Header />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
