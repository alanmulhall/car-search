export const APP_NAME = 'Qantas Loyalty';
export const APP_LOGO = '/static/logos/qantas-loyalty-logo.png';
export const QANTAS = 'Qantas';
export const QANTAS_LOGO = '/static/logos/logo-qantas-new.png';
