export const HOST_NAME = 'http://localhost:3000';
export const WEEKLY_OFFER = '/api/v1/offer';
export const MODEL = '/api/v1/model';
export const MODELS = '/api/v1/models';
export const MAKES = '/api/v1/makes';
