import * as types from '../constants/actionTypes';

const initialState = {};

const offer = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.WEEKLY_OFFER_UPDATE:
      return {
        ...state,
        ...action.payload
      };

    default:
      return state;
  }
};

export default offer;
