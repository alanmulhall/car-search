import { combineReducers } from 'redux';
import makes from './makes';
import model from './model';
import models from './models';
import offer from './offer';

const rootReducer = combineReducers({
  makes,
  model,
  models,
  offer
});

export default rootReducer;
