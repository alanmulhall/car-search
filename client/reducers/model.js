import * as types from '../constants/actionTypes';

const initialState = {};

const model = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.MODEL_UPDATE:
      return {
        ...state,
        ...action.payload
      };

    default:
      return state;
  }
};

export default model;
