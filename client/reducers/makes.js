import * as types from '../constants/actionTypes';

const initialState = [];

const makes = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.MAKES_UPDATE:
      return action.payload;

    default:
      return state;
  }
};

export default makes;
