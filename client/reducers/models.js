import * as types from '../constants/actionTypes';

const initialState = [];

const models = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.MODELS_UPDATE:
      return action.payload;

    default:
      return state;
  }
};

export default models;
