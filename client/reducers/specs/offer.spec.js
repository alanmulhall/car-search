import { weeklyOfferUpdate } from '../../actions/actionCreators';
import offer from '../offer';

const initialState = {};
const mockWeeklyOffer = {
  modelId: 520,
  review: 'The Mazda MX-5 is a traditional two-seat sports car, with a lightweight body and rear-wheel drive. It has a folding, fabric roof and is among the least expensive convertibles. This fourth-generation MX-5 is fantastic fun to drive. Motoring magazine Wheels named it Car of the Year for 2016.' // eslint-disable-line
};

describe('offer.js', () => {
  describe('Offer reducer', () => {
    it('returns the correct initial state', () => {
      expect(offer(undefined)).toEqual(initialState);
    });

    it('calling "weeklyOfferUpdate()" action creator updates the state', () => {
      const expectedState = mockWeeklyOffer;
      expect(offer(initialState, weeklyOfferUpdate(mockWeeklyOffer))).toEqual(
        expectedState
      );
    });
  });
});
