import { makesUpdate } from '../../actions/actionCreators';
import makes from '../makes';

const initialState = [];
const mockMakes = [
  {
    id: 10,
    name: 'Porsche'
  },
  {
    id: 20,
    name: 'Nissan'
  }
];

describe('makes.js', () => {
  describe('Model reducer', () => {
    it('returns the correct initial state', () => {
      expect(makes(undefined)).toEqual(initialState);
    });

    it('calling "makesUpdate()" action creator updates the state', () => {
      const expectedState = mockMakes;
      expect(makes(initialState, makesUpdate(mockMakes))).toEqual(
        expectedState
      );
    });
  });
});
