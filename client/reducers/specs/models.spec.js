import { modelsUpdate } from '../../actions/actionCreators';
import models from '../models';

const initialState = [];
const mockModels = [
  {
    id: 100,
    makeId: 10,
    name: '911 Carrera 4s',
    price: 297130,
    imageUrl: 'http://files1.porsche.com/filestore/image/multimedia/none/991-2nd-c4s-modelimage-sideshot/model/15bd09cf-553b-11e5-8c32-0019999cd470;s25/porsche-model.png' // eslint-disable-line
  },
  {
    id: 110,
    makeId: 10,
    name: 'Cayenne GTS',
    price: 171605,
    imageUrl: 'http://files3.porsche.com/filestore/image/multimedia/none/rd-2013-9pa-e2-2nd-gts-modelimage-sideshot/model/c287d350-5920-11e4-99aa-001a64c55f5c;s25/porsche-model.png' // eslint-disable-line
  }
];

describe('makes.js', () => {
  describe('Model reducer', () => {
    it('returns the correct initial state', () => {
      expect(models(undefined)).toEqual(initialState);
    });

    it('calling "modelsUpdate()"" action creator updates the state', () => {
      const expectedState = mockModels;
      expect(models(initialState, modelsUpdate(mockModels))).toEqual(
        expectedState
      );
    });
  });
});
