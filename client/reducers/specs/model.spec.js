import { modelUpdate } from '../../actions/actionCreators';
import model from '../model';

const initialState = {};
const mockModel = {
  imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
  makeId: 50,
  modelId: 520,
  name: 'MX-5',
  price: 90000
};

describe('model.js', () => {
  describe('Model reducer', () => {
    it('returns the correct initial state', () => {
      expect(model(undefined)).toEqual(initialState);
    });

    it('calling "modelUpdate()" action creator updates the state', () => {
      const expectedState = mockModel;
      expect(model(initialState, modelUpdate(mockModel))).toEqual(
        expectedState
      );
    });
  });
});
