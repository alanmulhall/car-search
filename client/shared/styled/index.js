import styled from 'styled-components';

export const BoxStyled = styled.div`
  background-color: rgba(255, 255, 255, 0.7);
  border: 5px solid #e40000;
  border-radius: 10px;
  overflow: hidden;
  padding: 16px 24px;
`;
