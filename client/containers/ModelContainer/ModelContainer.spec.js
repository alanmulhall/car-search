import React from 'react';
import { createMockStore } from 'redux-test-utils';
import ModelContainer from './ModelContainer';
import withShallowStore from '../../shared/tests/components/withShallowStore';

describe('<ModelContainer />', () => {
  const mockState = {
    model: {
      id: 520,
      imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
      makeId: 50,
      name: 'MX-5',
      price: 90000
    }
  };
  let store;
  let wrapper;

  beforeEach(() => {
    store = createMockStore(mockState);
    wrapper = withShallowStore(<ModelContainer />, store);
  });

  it('renders <Offer /> successfully', () => {
    expect(wrapper.find('Model')).toBeTruthy();
  });

  it('passes the correct props', () => {
    expect(wrapper.props().imageUrl).toEqual(mockState.model.imageUrl);
    expect(wrapper.props().makeId).toEqual(mockState.model.makeId);
    expect(wrapper.props().modelId).toEqual(mockState.model.modelId);
    expect(wrapper.props().name).toEqual(mockState.model.name);
    expect(wrapper.props().price).toEqual(mockState.model.price);
  });
});
