import { connect } from 'react-redux';
import Model from '../../components/Model';

const mapStateToProps = state => {
  return {
    id: state.model.id,
    imageUrl: state.model.imageUrl,
    makeId: state.model.makeId,
    name: state.model.name,
    price: state.model.price
  };
};

export default connect(mapStateToProps)(Model);
