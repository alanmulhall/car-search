import { connect } from 'react-redux';
import Search from '../../components/Search';

const mapStateToProps = state => {
  return {
    makes: state.makes,
    models: state.models
  };
};

export default connect(mapStateToProps)(Search);
