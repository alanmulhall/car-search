import React from 'react';
import { createMockStore } from 'redux-test-utils';
import SearchContainer from './SearchContainer';
import withShallowStore from '../../shared/tests/components/withShallowStore';

describe('<SearchContainer />', () => {
  const mockState = {
    makes: [
      {
        id: 10,
        name: 'Porsche'
      },
      {
        id: 20,
        name: 'Nissan'
      }
    ],
    models: [
      {
        id: 100,
        makeId: 10,
        name: '911 Carrera 4s',
        price: 297130,
        imageUrl: 'http://files1.porsche.com/filestore/image/multimedia/none/991-2nd-c4s-modelimage-sideshot/model/15bd09cf-553b-11e5-8c32-0019999cd470;s25/porsche-model.png' // eslint-disable-line
      },
      {
        id: 110,
        makeId: 10,
        name: 'Cayenne GTS',
        price: 171605,
        imageUrl: 'http://files3.porsche.com/filestore/image/multimedia/none/rd-2013-9pa-e2-2nd-gts-modelimage-sideshot/model/c287d350-5920-11e4-99aa-001a64c55f5c;s25/porsche-model.png' // eslint-disable-line
      },
      {
        id: 120,
        makeId: 10,
        name: 'Panamera 4S',
        price: 328160,
        imageUrl: 'http://files1.porsche.com/filestore/image/multimedia/none/970-g2-4s-modelimage-sideshot/model/a23b6da0-33b9-11e6-9225-0019999cd470;s25/porsche-model.png' // eslint-disable-line
      }
    ]
  };
  let store;
  let wrapper;

  beforeEach(() => {
    store = createMockStore(mockState);
    wrapper = withShallowStore(<SearchContainer />, store);
  });

  it('renders <Search /> successfully', () => {
    expect(wrapper.find('Search')).toBeTruthy();
  });

  it('passes the correct props', () => {
    expect(wrapper.props().makes).toEqual(mockState.makes);
    expect(wrapper.props().models).toEqual(mockState.models);
  });
});
