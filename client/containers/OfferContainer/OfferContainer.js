import { connect } from 'react-redux';
import Offer from '../../components/Offer';

const mapStateToProps = state => {
  return {
    imageUrl: state.model.imageUrl,
    modelId: state.model.id,
    name: state.model.name,
    review: state.offer.review
  };
};

export default connect(mapStateToProps)(Offer);
