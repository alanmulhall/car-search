import React from 'react';
import { createMockStore } from 'redux-test-utils';
import OfferContainer from './OfferContainer';
import withShallowStore from '../../shared/tests/components/withShallowStore';

describe('<OfferContainer />', () => {
  const mockState = {
    model: {
      id: 520,
      imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
      makeId: 50,
      name: 'MX-5',
      price: 90000
    },
    offer: {
      name: 'MX-5',
      review: 'This is a mock review.'
    }
  };
  let store;
  let wrapper;

  beforeEach(() => {
    store = createMockStore(mockState);
    wrapper = withShallowStore(<OfferContainer />, store);
  });

  it('renders <Offer /> successfully', () => {
    expect(wrapper.find('Offer')).toBeTruthy();
  });

  it('passes the correct props', () => {
    expect(wrapper.props().imageUrl).toEqual(mockState.model.imageUrl);
    expect(wrapper.props().modelId).toEqual(mockState.model.id);
    expect(wrapper.props().name).toEqual(mockState.offer.name);
    expect(wrapper.props().review).toEqual(mockState.offer.review);
  });
});
