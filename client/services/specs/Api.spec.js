import nock from 'nock';
import Api from '../Api';

describe('Api()', () => {
  describe('getWeeklyOffer()', () => {
    it('returns the correct data', async () => {
      nock('http://localhost:3000', { encodedQueryParams: true })
        .get('/api/v1/offer')
        .reply(200, {"modelId":520,"review":"The Mazda MX-5 is a traditional two-seat sports car, with a lightweight body and rear-wheel drive. It has a folding, fabric roof and is among the least expensive convertibles. This fourth-generation MX-5 is fantastic fun to drive. Motoring magazine Wheels named it Car of the Year for 2016."}); // eslint-disable-line

      const actualData = await Api.getWeeklyOffer();
      const expectedData = {"modelId":520,"review":"The Mazda MX-5 is a traditional two-seat sports car, with a lightweight body and rear-wheel drive. It has a folding, fabric roof and is among the least expensive convertibles. This fourth-generation MX-5 is fantastic fun to drive. Motoring magazine Wheels named it Car of the Year for 2016."}; // eslint-disable-line
      expect(actualData).toEqual(expectedData);
    });
  });

  describe('getModel()', () => {
    it('returns the correct data', async () => {
      nock('http://localhost:3000', { encodedQueryParams: true })
        .get('/api/v1/model/520')
        .reply(200, {"id":520,"makeId":50,"name":"MX-5","price":90000,"imageUrl":"https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg"}); // eslint-disable-line

      const actualData = await Api.getModel(520);
      const expectedData = {"id":520,"makeId":50,"name":"MX-5","price":90000,"imageUrl":"https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg"}; // eslint-disable-line
      expect(actualData).toEqual(expectedData);
    });
  });
});
