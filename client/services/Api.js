import 'isomorphic-fetch';
import {
  HOST_NAME,
  MAKES,
  MODEL,
  MODELS,
  WEEKLY_OFFER
} from '../constants/api';

class Api {
  static async getWeeklyOffer() {
    let result;
    try {
      const res = await fetch(`${HOST_NAME}${WEEKLY_OFFER}`);
      result = await res.json();
    } catch (e) {
      result = {};
    }
    return result;
  }

  static async getModel(modelId) {
    let result;
    try {
      const res = await fetch(`${HOST_NAME}${MODEL}/${modelId}`);
      result = await res.json();
    } catch (e) {
      result = {};
    }
    return result;
  }

  static async getModels() {
    let result;
    try {
      const res = await fetch(`${HOST_NAME}${MODELS}`);
      result = await res.json();
    } catch (e) {
      result = [];
    }
    return result;
  }

  static async getMakes() {
    let result;
    try {
      const res = await fetch(`${HOST_NAME}${MAKES}`);
      result = await res.json();
    } catch (e) {
      result = [];
    }
    return result;
  }
}

export default Api;
