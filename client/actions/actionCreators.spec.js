import nock from 'nock';
import {
  makesUpdate,
  makesFetch,
  modelFetch,
  modelsFetch,
  modelUpdate,
  modelsUpdate,
  weeklyOfferfetch,
  weeklyOfferUpdate
} from './actionCreators';
import * as types from '../constants/actionTypes';

describe('actionCreators.js', () => {
  describe('weeklyOfferUpdate()', () => {
    it('creates a action to update weekly offer', () => {
      const mockData = {
        modelId: 520,
        review: 'The Mazda MX-5 is a traditional two-seat sports car, with a lightweight body and rear-wheel drive. It has a folding, fabric roof and is among the least expensive convertibles. This fourth-generation MX-5 is fantastic fun to drive. Motoring magazine Wheels named it Car of the Year for 2016.' // eslint-disable-line
      };

      const expectedAction = {
        type: types.WEEKLY_OFFER_UPDATE,
        payload: mockData
      };
      expect(weeklyOfferUpdate(mockData)).toEqual(expectedAction);
    });
  });

  describe('weeklyOfferfetch()', () => {
    it('dispatches the correct actions', async () => {
      nock('http://localhost:3000')
        .get('/api/v1/offer')
        .reply(200, {"modelId":520,"review":"The Mazda MX-5 is a traditional two-seat sports car, with a lightweight body and rear-wheel drive. It has a folding, fabric roof and is among the least expensive convertibles. This fourth-generation MX-5 is fantastic fun to drive. Motoring magazine Wheels named it Car of the Year for 2016."}); // eslint-disable-line

      nock('http://localhost:3000')
        .get('/api/v1/model/520')
        .reply(200, {"id":520,"makeId":50,"name":"MX-5","price":90000,"imageUrl":"https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg"}); // eslint-disable-line

      const dispatch = jest.fn();
      await weeklyOfferfetch()(dispatch);
      expect(dispatch.mock.calls.length).toEqual(2);
      expect(dispatch.mock.calls[0][0].type).toEqual('WEEKLY_OFFER_UPDATE');
      expect(dispatch.mock.calls[1][0].type).toEqual('MODEL_UPDATE');
    });
  });

  describe('modelFetch()', () => {
    it('dispatches the correct actions', async () => {
      nock('http://localhost:3000', { encodedQueryParams: true })
        .get('/api/v1/model/502')
        .reply(404, { error: 'Route not found' });

      const dispatch = jest.fn();
      await modelFetch(502)(dispatch);
      expect(dispatch.mock.calls.length).toEqual(1);
      expect(dispatch.mock.calls[0][0].type).toEqual('MODEL_UPDATE');
    });
  });

  describe('modelUpdate()', () => {
    it('creates a action to update weekly offer', () => {
      const mockData = {
        id: 520,
        imageUrl: 'https://www.mazda.com.au/globalassets/vehicle-landing-pages/mx-5/videos-and-back-ups/mx-5_hero_video-fallback.jpg', // eslint-disable-line
        makeId: 50,
        name: 'MX-5',
        price: 90000
      };

      const expectedAction = {
        type: types.MODEL_UPDATE,
        payload: mockData
      };
      expect(modelUpdate(mockData)).toEqual(expectedAction);
    });
  });

  describe('makesFetch()', () => {
    it('dispatches the correct actions', async () => {
      nock('http://localhost:3000', { encodedQueryParams: true })
        .get('/api/v1/makes')
        .reply(200, [{"id":10,"name":"Porsche"},{"id":20,"name":"Nissan"},{"id":30,"name":"BMW"},{"id":40,"name":"Audi"},{"id":50,"name":"Mazda"}]); // eslint-disable-line

      const dispatch = jest.fn();
      await makesFetch()(dispatch);
      expect(dispatch.mock.calls.length).toEqual(1);
      expect(dispatch.mock.calls[0][0].type).toEqual('MAKES_UPDATE');
    });
  });

  describe('makesUpdate()', () => {
    it('creates a action to update makes', () => {
      const mockMakes = [
        {
          id: 10,
          name: 'Porsche'
        },
        {
          id: 20,
          name: 'Nissan'
        }
      ];

      const expectedAction = {
        type: types.MAKES_UPDATE,
        payload: mockMakes
      };
      expect(makesUpdate(mockMakes)).toEqual(expectedAction);
    });
  });

  describe('modelsFetch()', () => {
    it('dispatches the correct actions', async () => {
      nock('http://localhost:3000', { encodedQueryParams: true })
        .get('/api/v1/models')
        .reply(200, ["1f8b0800000000000013","ad956b6fdb361486ff4ae6afcd116fa22ed9b7064350a0bb6049810143511c93b4cd562255528edd14fbefa364bb9631bb6bd01a3040f110e2fb9cf725f5f7e799d5b31b46e9f5acc50fe6d5f8703d73d89ad9cdac66ecea16433001aff238bb9e75c1aa54e075c9445a665b5c9a37a1494b577ddfdd10b2b08d892ceb7c886a6532e5dbdd54ef8321e372d2ae9bdeb6465b24ce3b43ea9a01771a541ea1f5da34e33288569bb8f23d19e70893734d6bb50029c51c1833122a253850caeaf4533a2fe9cf914bb2df7af7aaac73cbd93fd77b4a769ef2163f19e7ccd5ddc3fd119195aca0f222a2781662d0c02913507708868fb4cbfe6bb48a57a5169282ac391d6873a86bc481168b5c49b990eaff68f979da3f70180c864e6805af12ee8f32b4a4b0e4f0553b918b79a1918210f37a002ca0e65c3ec74e3eb5931f015f1b5c1cc924a5f42c98cfd0374abb1129fd534f9421b8eebdc61e776891bcb9bf65f4b757b7ac602f69da91b2ec7d37d1c0cf6bb87b803f2759aabe4984b66d84b8c260f43816e4eef5ef2f890abe238c57745b72fa82be603427c144fb64487aef3697f427b2f0a1c59e2469e4e31a1bdb7f229524e73659c54806f346e3acc648a42c452979a54c35af44c12956f97c81422a2a0d939ca4e84a515256a611adc1d918d1a50087935688e91d228ead60c73e54e2421b369b4d366f3783c00cd7c90dd71bd7138d2d49d3a33bde116c9a5d0c2261104db0c91f09dafb304a24d803c2b241977c4c61b34b078b0d986d3f2c5d07d87947deab7073d82118a76d6fbd8b447d9469bf6cd7599dd97699c94a668ddf6443ba599e97351779c1795e9d82b3f3e07c12002e7f14393f902bbfeecc002e4ec07ff982bb6f0115df455d52c1739ee8d3ec09353f4f2d26d417cfdeb3a9c5813a1a8deebf761fea30d6f7e8dfeb76cecb52b0ead4ed7c1af3fcc87d2f27e0c525bbc75b325318d0691deca309e3a1dc5f36bb3b9417a528e470d62ac0b5b6102574e9e6f4c0a8a43c0500d299dff2323f1126a7319447610f0f4761f92543be4597603c1f5495871b207d9f9ef6ca445915832e5e6f79c14f75f1f3ba7efd0b262dabcf2a8bfbacb4f8a4f1909665e3e7d8608ca68fe4d1acac6a0ca41868eb96d0ed046f4192c7f4c5f1115201e6a83ec0badb15dead4cf0efc62a2c52c686e24ef1db7f01b3cbd8d50f090000"]); // eslint-disable-line

      const dispatch = jest.fn();
      await modelsFetch()(dispatch);
      expect(dispatch.mock.calls.length).toEqual(1);
      expect(dispatch.mock.calls[0][0].type).toEqual('MODELS_UPDATE');
    });
  });

  describe('modelsUpdate()', () => {
    it('creates a action to update models', () => {
      const mockModels = [
        {
          id: 100,
          makeId: 10,
          name: '911 Carrera 4s',
          price: 297130,
          imageUrl: 'http://files1.porsche.com/filestore/image/multimedia/none/991-2nd-c4s-modelimage-sideshot/model/15bd09cf-553b-11e5-8c32-0019999cd470;s25/porsche-model.png' // eslint-disable-line
        },
        {
          id: 110,
          makeId: 10,
          name: 'Cayenne GTS',
          price: 171605,
          imageUrl: 'http://files3.porsche.com/filestore/image/multimedia/none/rd-2013-9pa-e2-2nd-gts-modelimage-sideshot/model/c287d350-5920-11e4-99aa-001a64c55f5c;s25/porsche-model.png' // eslint-disable-line
        }
      ];

      const expectedAction = {
        type: types.MODELS_UPDATE,
        payload: mockModels
      };
      expect(modelsUpdate(mockModels)).toEqual(expectedAction);
    });
  });
});
