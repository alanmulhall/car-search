import * as types from '../constants/actionTypes';
import Api from '../services/Api';

export function weeklyOfferUpdate(data) {
  return {
    type: types.WEEKLY_OFFER_UPDATE,
    payload: data
  };
}

export function modelUpdate(data) {
  return {
    type: types.MODEL_UPDATE,
    payload: data
  };
}

export function modelsUpdate(data) {
  return {
    type: types.MODELS_UPDATE,
    payload: data
  };
}

export function weeklyOfferfetch() {
  return async dispatch => {
    const offer = await Api.getWeeklyOffer();
    const model = await Api.getModel(offer.modelId);
    dispatch(weeklyOfferUpdate(offer));
    dispatch(modelUpdate(model));
  };
}

export function modelFetch(id) {
  return async dispatch => {
    const model = await Api.getModel(id);
    dispatch(modelUpdate(model));
  };
}

export function modelsFetch() {
  return async dispatch => {
    const models = await Api.getModels();
    dispatch(modelsUpdate(models));
  };
}

export function makesUpdate(data) {
  return {
    type: types.MAKES_UPDATE,
    payload: data
  };
}

export function makesFetch() {
  return async dispatch => {
    const models = await Api.getMakes();
    dispatch(makesUpdate(models));
  };
}
