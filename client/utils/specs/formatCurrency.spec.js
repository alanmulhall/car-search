import formatCurrency from '../formatCurrency';

describe('formatCurrency()', () => {
  it('formats currency', () => {
    const expectedCurrency = '90,000.00';
    expect(formatCurrency(90000)).toEqual(expectedCurrency);
  });
});
