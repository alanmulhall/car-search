import dissoc from '../dissoc';

describe('dissoc()', () => {
  it('removes the given key from the object', () => {
    const mockObject = {
      id: 520,
      makeId: 50,
      name: 'MX-5'
    };

    const expectedObject = {
      makeId: 50,
      name: 'MX-5'
    };
    const actualObject = dissoc('id', mockObject);
    expect(actualObject).toEqual(expectedObject);
  });
});
