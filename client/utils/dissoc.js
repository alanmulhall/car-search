const dissoc = (key, obj) => {
  const copy = { ...obj };
  delete copy[key];
  return copy;
};

export default dissoc;
